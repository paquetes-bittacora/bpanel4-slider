/**
 * Script para el slider en la parte pública
 */

import Swiper from 'swiper/bundle';
import 'swiper/css/bundle';

document.addEventListener('DOMContentLoaded', function () {
    const sliders = document.querySelectorAll('.swiper-slider');

    sliders.forEach(function (element: HTMLElement) {
        const sliderConfiguration = JSON.parse(element.dataset.swiperConfiguration);
        new Swiper('#' + element.id, sliderConfiguration);
    });
});
