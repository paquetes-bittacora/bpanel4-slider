@php
    /**
     * @var \Bittacora\Bpanel4\Slider\Models\Slider $slider
     * @var \Bittacora\Bpanel4\Slider\Models\Slide $slide
     */
@endphp
<div id="swiper-slider-{{ $slider->getId() }}" class="swiper swiper-slider swiper-slider-{{ $sliderKey }}" data-swiper-configuration="{{ json_encode([
   'pagination'  => $slider->showPagination() ? ['el' => '.swiper-pagination', 'clickable' => true] : false,
   'delay' => $slider->getSlideDuration(),
   'autoplay' => [ 'delay' => $slider->getSlideDuration(), 'disableOnInteraction' => false, ],
   'navigation' => $slider->showArrows() ? [
      'prevEl' => '.swiper-button-prev',
      'nextEl' => '.swiper-button-next'
    ]  : false,
    'effect' => $slider->getEffect(),
]) }}">
    <div class="swiper-wrapper">
        @foreach($slider->slides()->where('active', true)->get()->all() as $slide)
            <div class="swiper-slide @if(null !== $slide->getMobileBackground()) has-mobile-background @endif">
                @if(null !== $slide->getBackground())
                    <img class="desktop-background" src="{{ $slide->getBackground()->getFullUrl() }}" alt="Diapositiva {{ $slider->getId() }}">
                @endif
                @if(null !== $slide->getMobileBackground())
                    <img class="mobile-background" src="{{ $slide->getMobileBackground()->getFullUrl() }}" alt="Diapositiva {{ $slider->getId() }}">
                @endif
                <div class="content-wrapper">
                    <div class="content">
                        {!! $slide->getContent() !!}
                    </div>
                </div>
                {{ Bp4Hook::execute('show-slide-additional-fields', $slide) }}
            </div>
        @endforeach
    </div>
    @if($slider->showArrows())
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    @endif
    @if($slider->showPagination())
        <div class="swiper-pagination"></div>
    @endif
</div>

<style>
    .swiper-slider-{{ $sliderKey }} .desktop-background,
    .swiper-slider-{{ $sliderKey }} .mobile-background {
        width: 100%;
        height: auto;
        object-fit: cover;
        object-position: center center;
    }

    .swiper-slider-{{ $sliderKey }} .content-wrapper {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    @media (max-width: {{ $slider->getMobileBreakpoint() - 1 }}px) {
        .swiper-slider-{{ $sliderKey }} .mobile-background {
            display: block;
        }

        .swiper-slider-{{ $sliderKey }} .desktop-background {
            display: none;
        }
    }

    @media (min-width: {{ $slider->getMobileBreakpoint()}}px) {
        .swiper-slider-{{ $sliderKey }} .mobile-background {
            display: none;
        }

        .swiper-slider-{{ $sliderKey }} .desktop-background {
            display: block;
        }
    }
</style>
