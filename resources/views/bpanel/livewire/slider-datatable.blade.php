<td>
    {{ $row->title }}
</td>
<td>
    {{ $row->slides->count() }}
</td>
<td class="align-middle">
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-user-'.$row->id))
</td>
<td class="align-middle">
    <div class="text-center">
        @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'bpanel4-slider.bpanel', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'el slider?'], key('user-buttons-'.$row->id))
    </div>
</td>
