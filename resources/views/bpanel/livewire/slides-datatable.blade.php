<td>
    {{ $row->title }}
</td>
<td class="align-middle">
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-user-'.$row->id))
</td>
<td>
    @livewire('language::datatable-languages', ['model' => $row, 'createRouteName' => 'bpanel4-slider.bpanel.slide.edit', 'editRouteName' => 'bpanel4-slider.bpanel.slide.edit',
    ], key('languages-blog-'.$row->id))
</td>
<td class="align-middle">
    <div class="text-center">
        @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'bpanel4-slider.bpanel.slide', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'la diapositiva?'], key('user-buttons-'.$row->id))
    </div>
</td>
