@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-slider::form.edit'))

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-slider::form.edit') }}</span>
            </h4>
        </div>

            <p></p>
        <div class="text-center mb-4 alert alert-primary font-bold mx-4">
            <i class="far fa-info-circle"></i> Para insertar este slider, pegue el siguiente código en una página, blog, etc: &#91;bpanel4-slider id={{ $slider->getId() }}&#93;&#91;/bpanel4-slider&#93;
        </div>
        @include('bpanel4-slider::bpanel._form', ['panelTitle' => __('bpanel4-slider::form.edit')])
    </div>

    <div class="card bcard mt-5">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-slider::form.edit-slides') }}</span>
            </h4>
        </div>
        <div class="p-4">
            @livewire('bpanel4-slider::slides-datatable', ['slider' => $slider])
            <div class="mt-3 text-right">
                <a href="{{ route('bpanel4-slider.bpanel.slide.create', ['slider' => $slider->getId()]) }}">
                    <button class="btn btn-success"><i class="fas fa-plus"></i> Añadir diapositiva</button>
                </a>
            </div>
        </div>
    </div>

    <div class="mt-4 text-center">
        <a href="{{ route('bpanel4-slider.bpanel.index') }}">< Volver al listado</a>
    </div>
@endsection

@push('scripts')
    @vite('vendor/bittacora/bpanel4-panel/resources/assets/js/livewire-sortable.js')
@endpush
