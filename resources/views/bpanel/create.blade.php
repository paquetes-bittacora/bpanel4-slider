@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-slider::form.new'))

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-slider::form.new') }}</span>
            </h4>
        </div>
        @include('bpanel4-slider::bpanel._form', ['panelTitle' => __('bpanel4-slider::form.new')])
    </div>

    <div class="mt-4 text-center">
        <a href="{{ route('bpanel4-slider.bpanel.index') }}">< Volver al listado</a>
    </div>
@endsection
