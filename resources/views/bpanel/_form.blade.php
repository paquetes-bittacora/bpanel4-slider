<form class="mt-lg-3" autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data">
    @csrf
    @livewire('form::input-text', ['name' => 'title', 'labelText' => __('bpanel4-slider::form.title'), 'required'=>true,
    'value' => old('title') ?? $slider?->getTitle() ])
    @livewire('form::input-number', ['name' => 'slide_duration', 'labelText' =>
    __('bpanel4-slider::form.slide_duration'), 'min' => 0, 'required'=>true, 'value' => old('slide_duration') ??
    $slider?->getSlideDuration() ?? 3000, 'labelWidth' => 3, 'fieldWidth' => 7 ])
    @livewire('form::input-number', ['name' => 'mobile_breakpoint', 'labelText' =>
    __('bpanel4-slider::form.mobile_breakpoint'), 'min' => 0, 'required'=>true, 'value' => old('mobile_breakpoint') ??
    $slider?->getMobileBreakpoint() ?? 640, 'labelWidth' => 3, 'fieldWidth' => 7 ])
    @livewire('form::select', ['name' => 'effect', 'selectedValues' => [$slider?->getEffect()], 'allValues' => [
    'slide' => 'Deslizar',
    'fade' => 'Fundir',
    'cube' => 'Cubo',
    'coverflow' => 'Coverflow',
    'flip' => 'Voltear',
    'creative' => 'Creativo'
    ], 'labelText' => __('bpanel4-slider::form.effect'), 'fieldWidth' => 7])
    @livewire('form::input-checkbox', ['name' => 'show_pagination', 'value' => 1, 'checked' => old('show_pagination') ??
    $slider?->showPagination(), 'labelText' => __('bpanel4-slider::form.show_pagination'), 'bpanelForm' => true])
    @livewire('form::input-checkbox', ['name' => 'show_arrows', 'value' => 1, 'checked' => old('show_arrows') ??
    $slider?->showarrows(), 'labelText' => __('bpanel4-slider::form.show_arrows'), 'bpanelForm' => true])
    @livewire('form::input-checkbox', ['name' => 'active', 'value' => 1, 'checked' => old('active') ??
    $slider?->isActive(), 'labelText' => __('bpanel4-slider::form.active'), 'bpanelForm' => true])
    <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
        @livewire('form::save-button',['theme'=>'save'])
        @livewire('form::save-button',['theme'=>'update'])
        @livewire('form::save-button',['theme'=>'reset'])
    </div>
    @if($slider?->getId() !== null)
        <input type="hidden" name="id" value="{{ $slider->getId() }}">
    @endif

    @if(isset($language))
        <input type="hidden" name="locale" value="{{ $language }}">
    @endif
</form>
