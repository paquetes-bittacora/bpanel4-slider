@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-slider::form.edit-slide'))

@section('content')
    @if(null === $slide)
        <p class="alert alert-success">
            {!!  __('bpanel4-slider::form.default_language') !!}
        </p>
    @endif
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-slider::form.edit-slide') }}</span>
            </h4>
            @if(null !== $slide)
                @include('language::partials.form-languages', ['model' => $slide, 'edit_route_name' => 'bpanel4-slider.bpanel.slide.edit',  'currentLanguage' => $language])
            @endif
        </div>
        @include('bpanel4-slider::bpanel.slides._form', ['panelTitle' => __('bpanel4-slider::form.edit-slide')])
    </div>
    <div class="mt-4 text-center">
        <a href="{{ route('bpanel4-slider.bpanel.edit', ['model' => $slider->id]) }}">< Volver al slider</a>
    </div>
@endsection
