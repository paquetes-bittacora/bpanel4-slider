<form class="mt-lg-3" autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data">
    @csrf
    @livewire('form::input-text', ['name' => 'title', 'labelText' => __('bpanel4-slider::form.title'), 'required'=>true,
    'value' => old('title') ?? $slide?->getTitle() ])
    @livewire('utils::tinymce-editor', ['name' => 'content', 'labelText' => __('bpanel4-slider::form.content'), 'value'
    => old('content') ?? $slide?->getContent() ])
    @if (null !== $slide?->getBackground())
        <div class="form-group form-row">
            <div class="col-sm-3 col-form-label text-sm-right">
                <label for="id-form-field-1" class="mb-0  ">
                    Fondo actual
                </label>
            </div>

            <div class="col-sm-7">
                <img src="{{ $slide->getBackground()->getFullUrl() }}" style="max-width: 500px;">
            </div>
        </div>
    @endif
    @livewire('form::input-file', ['name' => 'background', 'labelText' => 'Fondo',
    'maxFileCount' => 1, 'accept' => '.jpg,.jpeg,.png,.webp', 'allowedFileExtensions' => 'jpg,jpeg,png,webp' ])
    @if (null !== $slide?->getMobileBackground())
        <div class="form-group form-row">
            <div class="col-sm-3 col-form-label text-sm-right">
                <label for="id-form-field-1" class="mb-0  ">
                    Fondo actual (móviles)
                </label>
            </div>

            <div class="col-sm-7">
                <img src="{{ $slide->getMobileBackground()->getFullUrl() }}" style="max-width: 500px;">
                <div class="mt-2">
                    <a onclick="return confirm('¿Confirma que desea quitar la imagen?')" href="{{ route('bpanel4-slider.bpanel.remove-mobile-background', ['slide' => $slide]) }}" class="btn btn-danger"><i class="fas fa-times"></i> Quitar</a>
                </div>
            </div>
        </div>
    @endif
    @livewire('form::input-file', ['name' => 'mobile_background', 'labelText' => 'Fondo (dispositivos móviles)',
    'maxFileCount' => 1, 'accept' => '.jpg,.jpeg,.png,.webp', 'allowedFileExtensions' => 'jpg,jpeg,png,webp' ])
    {{ Bp4Hook::execute('slide-additional-fields', $slide) }}
    @livewire('form::input-text', ['name' => 'link', 'labelText' => __('bpanel4-slider::form.link'),
    'value' => old('link') ?? $slide?->getLink() ])
    @livewire('form::input-checkbox', ['name' => 'new_tab', 'value' => 1, 'checked' => old('new_tab') ??
    $slide?->getNewTab(), 'labelText' => __('bpanel4-slider::form.new_tab'), 'bpanelForm' => true])
    @livewire('form::input-checkbox', ['name' => 'active', 'value' => 1, 'checked' => old('active') ??
    $slide?->isActive(), 'labelText' => __('bpanel4-slider::form.active'), 'bpanelForm' => true])
    <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
        @livewire('form::save-button',['theme'=>'save'])
        @livewire('form::save-button',['theme'=>'update'])
        @livewire('form::save-button',['theme'=>'reset'])
    </div>
    @if($slide?->getId() !== null)
        <input type="hidden" name="id" value="{{ $slide->getId() }}">
    @endif
    <input type="hidden" name="slider_id" value="{{ $slider->getId() }}">

    @if(isset($language))
        <input type="hidden" name="locale" value="{{ $language }}">
    @endif
</form>