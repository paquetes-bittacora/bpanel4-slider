<?php

declare(strict_types=1);

return [
    'new' => 'Nuevo slider',
    'title' => 'Título',
    'active' => 'Activado',
    'show_pagination' => 'Mostrar paginación',
    'show_arrows' => 'Mostrar flechas',
    'slide_duration' => 'Duración de las diapositivas (en ms)',
    'mobile_breakpoint' => 'Breakpoint CSS para dispositivos móiviles (en px)',
    'effect' => 'Efecto de transición',
    'edit' => 'Editar slider',
    'error-creating' => 'Ocurrió un error al crear el slider',
    'error-updating' => 'Ocurrió un error al actualizar el slider',
    'updated' => 'Slider actualizado',
    'created' => 'Slider creado',
    'saved' => 'Slider guardado',
    'deleted' => 'Slider eliminado',
    'edit-slides' => 'Editar diapositivas',
    'edit-slide' => 'Editar diapositiva',
    'new_tab' => 'Abrir enlace en nueva pestaña',
    'link' => 'Enlace',
    'content' => 'Contenido',
    'slide-saved' => 'Diapositiva guardada',
    'slide-not-created' => 'Error al guardar la diapositiva',
    'slide-deleted' => 'Diapositiva eliminada',
    'default_language' => '<i class="fas fa-info"></i> Al crear una nueva diapositiva, se creará en el idioma principal. Si desea añadir traducciones, guarde primero la diapositiva en el idioma principal.',
];
