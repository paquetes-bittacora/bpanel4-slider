<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Tests\Unit;

use Bittacora\Bpanel4\Slider\Actions\CreateSlider;
use Bittacora\Bpanel4\Slider\Contracts\Repository\SliderRepository;
use Bittacora\Bpanel4\Slider\Dtos\SliderDto;
use Mockery;
use Tests\TestCase;

final class CreateSliderTest extends TestCase
{
    use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testCreaUnSlider(): void
    {
        $dto = $this->getDto();
        $sliderRepository = Mockery::mock(SliderRepository::class);
        $sliderRepository->shouldReceive('save')->once();
        $createSlider = new CreateSlider($sliderRepository);
        $createSlider->execute($dto);
    }

    private function getDto(): SliderDto
    {
        return new SliderDto(
            title: 'Título del slider',
            showPagination: true,
            showArrows: true,
            slideDuration: 3000,
            mobileBreakpoint: 640,
            effect: 'slide',
            active: true,
        );
    }
}
