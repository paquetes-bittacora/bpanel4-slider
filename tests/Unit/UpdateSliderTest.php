<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Tests\Unit;

use Bittacora\Bpanel4\Slider\Actions\UpdateSlider;
use Bittacora\Bpanel4\Slider\Contracts\Repository\SliderRepository;
use Bittacora\Bpanel4\Slider\Dtos\SliderDto;
use Bittacora\Bpanel4\Slider\Models\Slider;
use Mockery;
use Tests\TestCase;

final class UpdateSliderTest extends TestCase
{
    use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

    public function testCreaUnSlider(): void
    {
        $dto = $this->getDto();
        $sliderRepository = Mockery::mock(SliderRepository::class);
        $sliderRepository->shouldReceive('getById')->with(1)->andReturn(new Slider());
        $sliderRepository->shouldReceive('save')->once();
        $createSlider = new UpdateSlider($sliderRepository);
        $createSlider->execute($dto);
    }

    private function getDto(): SliderDto
    {
        return new SliderDto(
            title: 'Título del slider',
            id: 1,
            showPagination: true,
            showArrows: true,
            slideDuration: 3000,
            mobileBreakpoint: 640,
            effect: 'slide',
            active: true,
        );
    }
}
