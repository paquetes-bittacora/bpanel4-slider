<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Tests\Unit;

use Bittacora\Bpanel4\Slider\Actions\CreateSlide;
use Bittacora\Bpanel4\Slider\Actions\UpdateSlide;
use Bittacora\Bpanel4\Slider\Contracts\Repository\SlideRepository;
use Bittacora\Bpanel4\Slider\Dtos\SlideDto;
use Bittacora\Bpanel4\Slider\Models\Slide;
use Mockery;
use Tests\TestCase;

final class UpdateSlideTest extends TestCase
{
    public function testCreaUnaDiapositiva(): void
    {
        $dto = $this->getDto();
        $slideRepository = Mockery::mock(SlideRepository::class);
        $slideRepository->shouldReceive('getById')->with(12)->once()->andReturn(new Slide());
        $slideRepository->shouldReceive('save')->once();
        $createSlide = new UpdateSlide($slideRepository);
        $createSlide->execute($dto);
    }

    private function getDto(): SlideDto
    {
        return new SlideDto(
            title: 'Diapositiva',
            sliderId: 1,
            content: '{"es": "Probando"}' ,
            id: 12,
            link: 'https://example.com/',
            newTab: true,
            active: true,
        );
    }
}