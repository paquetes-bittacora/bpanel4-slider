<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Contracts\Repository;

use Bittacora\Bpanel4\Slider\Models\Slider;

interface SliderRepository
{
    public function save(Slider $slider): void;

    public function getById(int $id): Slider;
}
