<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Contracts\Repository;

use Bittacora\Bpanel4\Slider\Models\Slide;

interface SlideRepository
{
    public function save(Slide $slide): void;

    public function getById(?int $id): Slide;
}