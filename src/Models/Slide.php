<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Translatable\HasTranslations;

/**
 * Bittacora\Bpanel4\Slider\Models\Slide
 *
 * @property int $id
 * @property int $slider_id
 * @property string $title
 * @property string $content
 * @property string $background
 * @property string|null $mobile_background
 * @property mixed|null $link
 * @property int|bool $new_tab
 * @property bool|int $active
 * @property int|null $order_column
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Slide newModelQuery()
 * @method static Builder|Slide newQuery()
 * @method static Builder|Slide query()
 * @method static Builder|Slide whereActive($value)
 * @method static Builder|Slide whereBackground($value)
 * @method static Builder|Slide whereContent($value)
 * @method static Builder|Slide whereCreatedAt($value)
 * @method static Builder|Slide whereId($value)
 * @method static Builder|Slide whereLink($value)
 * @method static Builder|Slide whereMobileBackground($value)
 * @method static Builder|Slide whereNewTab($value)
 * @method static Builder|Slide whereOrderColumn($value)
 * @method static Builder|Slide whereSliderId($value)
 * @method static Builder|Slide whereTitle($value)
 * @method static Builder|Slide whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Slide extends Model implements HasMedia
{
    use HasTranslations;
    use InteractsWithMedia;

    /** @var string $table */
    public $table = 'sliders_slides';

    /** @var string[] $translatable */
    public array $translatable = ['title', 'content', 'link'];

    public function getId(): int
    {
        return $this->id;
    }

    public function getSliderId(): int
    {
        return $this->slider_id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getBackground(): ?Media
    {
        return $this->getMedia('background')->first();
    }

    public function getMobileBackground(): ?Media
    {
        return $this->getMedia('mobile_background')->first();
    }

    public function getLink(): mixed
    {
        return $this->link;
    }

    public function setLink(mixed $link): void
    {
        $this->link = $link;
    }

    public function getNewTab(): ?bool
    {
        return (bool) $this->new_tab;
    }

    public function setNewTab(bool $new_tab): void
    {
        $this->new_tab = $new_tab;
    }

    public function isActive(): bool
    {
        return (bool) $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function slider(): BelongsTo
    {
        return $this->belongsTo(Slider::class);
    }
}
