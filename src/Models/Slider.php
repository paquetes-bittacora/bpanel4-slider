<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Spatie\Translatable\HasTranslations;

/**
 * Bittacora\Bpanel4\Slider\Models\Slider
 *
 * @property int $id
 * @property mixed $title
 * @property int|bool $show_pagination
 * @property int|bool $show_arrows
 * @property int $slide_duration
 * @property int $mobile_breakpoint
 * @property string $effect
 * @property int|bool $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Slider newModelQuery()
 * @method static Builder|Slider newQuery()
 * @method static Builder|Slider query()
 * @method static Builder|Slider whereActive($value)
 * @method static Builder|Slider whereCreatedAt($value)
 * @method static Builder|Slider whereEffect($value)
 * @method static Builder|Slider whereId($value)
 * @method static Builder|Slider whereMobileBreakpoint($value)
 * @method static Builder|Slider whereShowArrows($value)
 * @method static Builder|Slider whereShowPagination($value)
 * @method static Builder|Slider whereSlideDuration($value)
 * @method static Builder|Slider whereTitle($value)
 * @method static Builder|Slider whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Slider extends Model
{
    use HasTranslations;
    use SoftDeletes;

    /** @var string[] $translatable */
    public $translatable = ['title'];

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getTitle(): mixed
    {
        return $this->title;
    }

    public function setTitle(mixed $title): void
    {
        $this->title = $title;
    }

    public function showPagination(): bool
    {
        return (bool) $this->show_pagination;
    }

    public function setShowPagination(bool $show_pagination): void
    {
        $this->show_pagination = $show_pagination;
    }

    public function showArrows(): bool
    {
        return (bool) $this->show_arrows;
    }

    public function setShowArrows(bool $show_arrows): void
    {
        $this->show_arrows = $show_arrows;
    }

    public function getSlideDuration(): int
    {
        return $this->slide_duration;
    }

    public function setSlideDuration(int $slide_duration): void
    {
        $this->slide_duration = $slide_duration;
    }

    public function getMobileBreakpoint(): int
    {
        return $this->mobile_breakpoint;
    }

    public function setMobileBreakpoint(int $mobile_breakpoint): void
    {
        $this->mobile_breakpoint = $mobile_breakpoint;
    }

    public function getEffect(): string
    {
        return $this->effect;
    }

    public function setEffect(string $effect): void
    {
        $this->effect = $effect;
    }

    public function isActive(): bool
    {
        return (bool) $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function slides(): HasMany
    {
        return $this->hasMany(Slide::class);
    }
}
