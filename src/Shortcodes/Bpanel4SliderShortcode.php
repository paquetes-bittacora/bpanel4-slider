<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Shortcodes;

use Bittacora\Bpanel4\Slider\Contracts\Repository\SliderRepository;
use Illuminate\Contracts\View\Factory;

final class Bpanel4SliderShortcode
{
    public function __construct(
        private readonly SliderRepository $sliderRepository,
        private readonly Factory $view,
    ) {
    }

    public function register($shortcode, $content, $compiler, $name, $viewData): string
    {
        $sliderId = $shortcode->toArray()['id'];
        $slider = $this->sliderRepository->getById((int)$sliderId);
        if (!$slider->isActive()) {
            return '';
        }
        return $this->view->make('bpanel4-slider::shortcodes.slider', [
            'slider' => $slider,
            'sliderKey' => bin2hex(random_bytes(2)),
        ])->render();
    }
}