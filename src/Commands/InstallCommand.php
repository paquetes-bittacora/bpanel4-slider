<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class InstallCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'bpanel4-slider:install';

    /**
     * @var string|null
     */
    protected $description = 'Instala el paquete para gestionar los sliders.';
    /**
     * @var string[]
     */
    private const PERMISSIONS = [
        'index',
        'create',
        'show',
        'edit',
        'destroy',
        'store',
        'update',
    ];

    /**
     * @throws \JsonException
     */
    public function handle(AdminMenu $adminMenu): void
    {
        $this->comment('Instalando el módulo Bpanel4 Slider...');
        $this->createMenuEntries($adminMenu);
        $this->createTabs();
        $this->giveAdminPermissions();
        $this->addJsDependencies();
        $this->registerModuleJs();
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'bpanel4-slider.bpanel.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }
    }

    private function createTabs(): void
    {
        Tabs::createItem(
            'bpanel4-slider.bpanel.index',
            'bpanel4-slider.bpanel.index',
            'bpanel4-slider.bpanel.index',
            'Listado',
            'fa fa-list'
        );
        Tabs::createItem(
            'bpanel4-slider.bpanel.index',
            'bpanel4-slider.bpanel.create',
            'bpanel4-slider.bpanel.create',
            'Nuevo',
            'fa fa-plus'
        );
    }

    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo al menú de administración...');

        $adminMenu->createGroup('contents', 'Contenidos', 'fas fa-text');
        $adminMenu->createModule(
            'contents',
            'bpanel4-slider.bpanel',
            'Sliders',
            'fas fa-images'
        );
        $adminMenu->createAction(
            'bpanel4-slider.bpanel',
            'Listado',
            'index',
            'fas fa-list'
        );
        $adminMenu->createAction(
            'bpanel4-slider.bpanel',
            'Nuevo',
            'create',
            'fas fa-plus'
        );
    }

    public function addJsDependencies(): void
    {
        $path = base_path('package.json');

        $packageJson = json_decode(file_get_contents($path), true, 512, JSON_THROW_ON_ERROR);

        if (!isset($packageJson['dependencies']['swiper'])) {
            $packageJson['dependencies']['swiper'] = '^9.4.1';
            file_put_contents($path, json_encode($packageJson, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT));
            $this->warn('El paquete bPanel 4 Slider necesita que ejecutes npm install');
        }
    }

    private function registerModuleJs(): void
    {
        $fileName = 'resources/bpanel4/assets/js/app.public.ts';

        if (!is_file($fileName)) {
            $fileName = 'resources/bpanel4/assets/js/app.public.js';
        }

        $path = base_path($fileName);
        $originalContent = file_get_contents($path);

        if (str_contains($originalContent, 'bPanel 4 slider')) {
            return;
        }

        $newContent = $originalContent . "\n\n// bPanel 4 slider\nimport '/vendor/bittacora/bpanel4-slider/resources/assets/js/bpanel4-slider.ts'";


        file_put_contents($path, $newContent);
    }
}
