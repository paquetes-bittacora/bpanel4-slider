<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Dtos;

use Bittacora\Dtos\Dto;

final class SliderDto extends Dto
{
    public function __construct(
        public readonly string $title,
        public readonly ?int $id = null,
        public readonly bool $showPagination = false,
        public readonly bool $showArrows = false,
        public readonly int $slideDuration = 3000,
        public readonly int $mobileBreakpoint = 640,
        public readonly string $effect = 'slide',
        public readonly bool $active = false,
    ) {
    }
}
