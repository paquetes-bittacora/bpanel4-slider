<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Dtos;

use Bittacora\Dtos\Dto;

final class SlideDto extends Dto
{
    public function __construct(
        public readonly string $title,
        public readonly int $sliderId,
        public readonly ?string $content = '',
        public readonly ?int $id = null,
        public readonly ?string $link = null,
        public readonly bool $newTab = false,
        public readonly bool $active = true,
    ) {
    }
}
