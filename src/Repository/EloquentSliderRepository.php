<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Repository;

use Bittacora\Bpanel4\Slider\Contracts\Repository\SliderRepository;
use Bittacora\Bpanel4\Slider\Models\Slider;

final class EloquentSliderRepository implements SliderRepository
{
    public function save(Slider $slider): void
    {
        $slider->save();
    }

    public function getById(int $id): Slider
    {
        return Slider::whereId($id)->firstOrFail();
    }
}
