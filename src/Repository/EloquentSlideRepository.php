<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Repository;

use Bittacora\Bpanel4\Slider\Contracts\Repository\SlideRepository;
use Bittacora\Bpanel4\Slider\Models\Slide;

final class EloquentSlideRepository implements SlideRepository
{
    public function save(Slide $slide): void
    {
        $slide->save();
    }

    public function getById(?int $id): Slide
    {
        return Slide::whereId($id)->firstOrFail();
    }
}