<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Actions;

use Bittacora\Bpanel4\Slider\Contracts\Repository\SliderRepository;
use Bittacora\Bpanel4\Slider\Dtos\SliderDto;
use Bittacora\Bpanel4\Slider\Models\Slider;

final class CreateSlider
{
    public function __construct(private readonly SliderRepository $sliderRepository)
    {
    }

    public function execute(SliderDto $dto): Slider
    {
        $slider = new Slider();

        $slider->setTitle($dto->title);
        $slider->setShowPagination($dto->showPagination);
        $slider->setShowArrows($dto->showArrows);
        $slider->setSlideDuration($dto->slideDuration);
        $slider->setMobileBreakpoint($dto->mobileBreakpoint);
        $slider->setEffect($dto->effect);
        $slider->setActive($dto->active);

        $this->sliderRepository->save($slider);

        return $slider;
    }
}
