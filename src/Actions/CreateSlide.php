<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Actions;

use Bittacora\Bpanel4\Slider\Contracts\Repository\SlideRepository;
use Bittacora\Bpanel4\Slider\Dtos\SlideDto;
use Bittacora\Bpanel4\Slider\Models\Slide;

final class CreateSlide
{
    public function __construct(private readonly SlideRepository $slideRepository)
    {
    }

    public function execute(SlideDto $dto): Slide
    {
        $slide = new Slide();
        $slide->setTitle($dto->title);
        $slide->setContent($dto->content);
        $slide->setLink($dto->link);
        $slide->setNewTab($dto->newTab);
        $slide->setActive($dto->active);
        $slide->slider()->associate($dto->sliderId);
        $this->slideRepository->save($slide);
        return $slide;
    }
}