<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Actions;

use Bittacora\Bpanel4\Slider\Contracts\Repository\SliderRepository;
use Bittacora\Bpanel4\Slider\Dtos\SliderDto;
use Bittacora\Bpanel4\Slider\Models\Slider;
use RuntimeException;

final class UpdateSlider
{
    public function __construct(private readonly SliderRepository $sliderRepository)
    {
    }

    public function execute(SliderDto $dto): Slider
    {
        if (null === $dto->id) {
            throw new RuntimeException('No se puede actualizar un slider si no se indica su ID');
        }

        $slider = $this->sliderRepository->getById($dto->id);

        $slider->setTitle($dto->title);
        $slider->setShowPagination($dto->showPagination);
        $slider->setShowArrows($dto->showArrows);
        $slider->setSlideDuration($dto->slideDuration);
        $slider->setMobileBreakpoint($dto->mobileBreakpoint);
        $slider->setEffect($dto->effect);
        $slider->setActive($dto->active);

        $this->sliderRepository->save($slider);

        return $slider;
    }
}
