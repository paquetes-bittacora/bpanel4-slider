<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Http\Controllers;

use Bittacora\Bpanel4\Slider\Actions\CreateSlide;
use Bittacora\Bpanel4\Slider\Actions\UpdateSlide;
use Bittacora\Bpanel4\Slider\Dtos\SlideDto;
use Bittacora\Bpanel4\Slider\Events\SlideSaved;
use Bittacora\Bpanel4\Slider\Models\Slide;
use Bittacora\Bpanel4\Slider\Models\Slider;
use Exception;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Events\Dispatcher;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Redirector;

final class SlideAdminController
{
    public function __construct(
        private readonly Factory $view,
        private readonly UrlGenerator $urlGenerator,
        private readonly Redirector $redirector,
        private readonly Translator $translator,
        private readonly ExceptionHandler $exceptionHandler,
        private readonly Dispatcher $dispatcher,
    ) {
    }

    public function create(Slider $slider, string $locale = 'es'): View
    {
        return $this->view->make('bpanel4-slider::bpanel.slides.edit', [
            'action' => $this->urlGenerator->route('bpanel4-slider.bpanel.slide.store', [
                'slider' => $slider,
            ]),
            'slide' => null,
            'slider' => $slider,
            'language' => $locale,
             ]);
    }

    public function uploadImages(Slide $slide, Request $request): void
    {
        $this->uploadImage($slide, $request->file('background'), 'background');
        $this->uploadImage($slide, $request->file('mobile_background'), 'mobile_background');
    }

    public function store(Request $request, Slider $slider, CreateSlide $createSlide): RedirectResponse
    {
        try {
            $requestData = $request->all();
            $requestData['content'] ??= '';
            $slide = $createSlide->execute(SlideDto::fromArray($requestData));
            $this->uploadImages($slide, $request);

            $this->dispatcher->dispatch(new SlideSaved($slide, $request));

            if ($request->has('save')) {
                return $this->redirector->route('bpanel4-slider.bpanel.edit', ['model' => $slider])
                    ->with(['alert-success' => $this->translator->get('bpanel4-slider::form.slide-saved')]);
            }

            return $this->redirector->route('bpanel4-slider.bpanel.slide.edit', ['model' => $slide])
                ->with(['alert-success' => $this->translator->get('bpanel4-slider::form.slide-saved')]);
        } catch (Exception $exception) {
            $this->exceptionHandler->report($exception);
            return $this->redirector->back()->withInput()->with([
                'alert-danger' => $this->translator->get('bpanel4-slider::form.slide-not-created'),
            ]);
        }
    }

    public function edit(Slide $model, string $locale = 'es'): View
    {
        $model->setLocale($locale);

        return $this->view->make('bpanel4-slider::bpanel.slides.edit', [
            'action' => $this->urlGenerator->route('bpanel4-slider.bpanel.slide.update', [
                'slide' => $model,
                'locale' => $locale,
            ]),
            'slide' => $model,
            'slider' => $model->slider()->firstOrFail(),
            'language' => $locale,
        ]);
    }

    public function update(Request $request, UpdateSlide $updateSlide, Slide $slide, string $locale = 'es'): RedirectResponse
    {
        try {
            $updateSlide->execute(SlideDto::fromArray($request->all()), $locale);
            $this->uploadImages($slide, $request);

            $this->dispatcher->dispatch(new SlideSaved($slide, $request));
            
            if ($request->has('save')) {
                return $this->redirector->route('bpanel4-slider.bpanel.edit', [
                    'model' => $slide->slider()->firstOrFail(),
                ])->with(['alert-success' => $this->translator->get('bpanel4-slider::form.saved')]);
            }
            return $this->redirector->route('bpanel4-slider.bpanel.slide.edit', [
                'model' => $slide,
                'locale' => $locale,
            ])->with(['alert-success' => $this->translator->get('bpanel4-slider::form.saved')]);
        } catch (Exception $exception) {
            $this->exceptionHandler->report($exception);
            return $this->redirector->back()->withInput()->with([
                'alert-danger' => $this->translator->get('bpanel4-slider::form.slide-not-created'),
            ]);
        }
    }

    private function uploadImage(Slide $slide, mixed $file, string $collection): void
    {
        if (!$file instanceof UploadedFile) {
            return;
        }

        $slide->clearMediaCollection($collection);

        $slide->addMedia($file)->toMediaCollection($collection, 'images');
    }

    public function destroy(Slide $slide): RedirectResponse
    {
        $slide->delete();

        return $this->redirector->back()->with([
            'alert-success' => $this->translator->get('bpanel4-slider::form.slide-deleted'),
        ]);
    }
}
