<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Slider\Actions\CreateSlider;
use Bittacora\Bpanel4\Slider\Actions\UpdateSlider;
use Bittacora\Bpanel4\Slider\Dtos\SliderDto;
use Bittacora\Bpanel4\Slider\Models\Slide;
use Bittacora\Bpanel4\Slider\Models\Slider;
use Exception;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class SliderAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly UrlGenerator $urlGenerator,
        private readonly Redirector $redirector,
        private readonly ExceptionHandler $exceptionHandler,
        private readonly Translator $translator,
    ) {
    }

    public function index(): View
    {
        return $this->view->make('bpanel4-slider::bpanel.index');
    }

    public function create(): View
    {
        return $this->view->make('bpanel4-slider::bpanel.create', [
            'action' => $this->urlGenerator->route('bpanel4-slider.bpanel.store'),
            'slider' => null,
        ]);
    }

    public function store(Request $request, CreateSlider $createSlider): RedirectResponse
    {
        try {
            $slider = $createSlider->execute(SliderDto::fromArray($request->all()));

            if ($request->has('save')) {
                return $this->redirector->route('bpanel4-slider.bpanel.index')
                    ->with(['alert-success' => $this->translator->get('bpanel4-slider::form.saved')]);
            }

            return $this->redirector->route('bpanel4-slider.bpanel.edit', ['slider' => $slider])
                ->with(['alert-success' => $this->translator->get('bpanel4-slider::form.saved')]);
        } catch (Exception $exception) {
            $this->exceptionHandler->report($exception);
            return $this->redirector->back()->withInput()->with([
                'alert-danger' => $this->translator->get('bpanel4-slider::form.error-creating'),
            ]);
        }
    }

    public function edit(Slider $model): View
    {
        return $this->view->make('bpanel4-slider::bpanel.edit', [
            'action' => $this->urlGenerator->route('bpanel4-slider.bpanel.update', ['slider' => $model]),
            'slider' => $model,
        ]);
    }

    public function update(Request $request, UpdateSlider $updateSlider): RedirectResponse
    {
        try {
            $updateSlider->execute(SliderDto::fromArray($request->all()));

            if ($request->has('save')) {
                return $this->redirector->route('bpanel4-slider.bpanel.index')
                    ->with(['alert-success' => $this->translator->get('bpanel4-slider::form.saved'),]);
            }

            return $this->redirector->back()
                ->with(['alert-success' => $this->translator->get('bpanel4-slider::form.updated')]);
        } catch (Exception $exception) {
            $this->exceptionHandler->report($exception);
            return $this->redirector->back()->withInput()->with([
                'alert-danger' => $this->translator->get('bpanel4-slider::form.error-updating'),
            ]);
        }
    }

    public function destroy(Slider $slider): RedirectResponse
    {
        $slider->delete();

        return $this->redirector->back()->with([
            'alert-success' => $this->translator->get('bpanel4-slider::form.deleted')
        ]);
    }

    public function removeMobileBackground(Slide $slide): RedirectResponse
    {
        $slide->getMobileBackground()?->delete();
        $slide->save();
        return $this->redirector->back()->with('alert-success', 'Fondo para móviles eliminado');
    }
}
