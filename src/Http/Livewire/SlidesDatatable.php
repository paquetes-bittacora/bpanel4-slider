<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Http\Livewire;

use Bittacora\Bpanel4\Slider\Models\Slide;
use Bittacora\Bpanel4\Slider\Models\Slider;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class SlidesDatatable extends DataTableComponent
{
    public bool $reordering = true;

    public string $reorderingMethod = 'reorder';

    public Slider $slider;

    /** @var array<int> $selectedKeys */
    public array $selectedKeys = [];

    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Título', 'title'),
            Column::make('Activo', 'active')->view('bpanel4-slider::bpanel.livewire.datatable-fields.slide-active'),
            Column::make('Idiomas', 'id')->view('bpanel4-slider::bpanel.livewire.datatable-fields.slide-languages'),
            Column::make('Acciones', 'id')->view('bpanel4-slider::bpanel.livewire.datatable-fields.slide-actions'),
        ];
    }

    /**
     * @return Builder<Slide>
     */
    public function query(): Builder
    {
        return Slide::query()
            ->where('slider_id', $this->slider->id)
            ->orderBy('order_column', 'ASC')
            ->when(
                $this->getAppliedFilterWithValue('search'),
                fn (Builder $query, int|string|null $term = null): Builder => $query
                ->where('title', 'like', '%' . $term . '%')
            );
    }

    public function rowView(): string
    {
        return 'bpanel4-slider::bpanel.livewire.slides-datatable';
    }

    /**
     * @return array{ bulkDelete: string }
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        if ([] !== $this->selectedKeys()) {
            Slide::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }

    /**
     * @param array<array{value: int, order: int}> $list
     */
    public function reorder(array $list): void
    {
        foreach ($list as $item) {
            Slide::where('id', $item['value'])->update(['order_column' => $item['order']]);
        }
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
