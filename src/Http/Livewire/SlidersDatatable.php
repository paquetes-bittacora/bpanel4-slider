<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Http\Livewire;

use Bittacora\Bpanel4\Slider\Models\Slider;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class SlidersDatatable extends DataTableComponent
{
    /** @var array<int> $selectedKeys */
    public array $selectedKeys = [];

    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Título', 'title'),
            Column::make('Nº diapositivas', 'id')->format(fn($value, Slider $row, Column $column) => $row->slides->count() ),
            Column::make('Activo', 'active')->view('bpanel4-slider::bpanel.livewire.datatable-fields.slider-active'),
            Column::make('Acciones', 'id')->view('bpanel4-slider::bpanel.livewire.datatable-fields.slider-actions'),
        ];
    }

    /**
     * @return Builder<Slider>
     */
    public function query(): Builder
    {
        /** @var Builder<Slider> */
        return Slider::query()
            ->with('slides')
            ->orderBy('created_at', 'DESC')
            ->when(
                $this->getAppliedFilterWithValue('search'),
                fn ($query, $term) => $query
                ->where('title', 'like', '%' . $term . '%')
            );
    }

    public function rowView(): string
    {
        return 'bpanel4-slider::bpanel.livewire.slider-datatable';
    }

    /**
     * @return string[]
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        if ([] !== $this->selectedKeys()) {
            Slider::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }

    public function setTableRowClass(Slider $row): string
    {
        return 'rappasoft-centered-row';
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
