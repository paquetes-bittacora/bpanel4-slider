<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider;

use Bittacora\Bpanel4\Slider\Commands\InstallCommand;
use Bittacora\Bpanel4\Slider\Contracts\Repository\SlideRepository;
use Bittacora\Bpanel4\Slider\Contracts\Repository\SliderRepository;
use Bittacora\Bpanel4\Slider\Http\Livewire\SlidersDatatable;
use Bittacora\Bpanel4\Slider\Http\Livewire\SlidesDatatable;
use Bittacora\Bpanel4\Slider\Repository\EloquentSlideRepository;
use Bittacora\Bpanel4\Slider\Repository\EloquentSliderRepository;
use Bittacora\Bpanel4\Slider\Shortcodes\Bpanel4SliderShortcode;
use Illuminate\Support\ServiceProvider;
use Livewire\LivewireManager;
use Webwizo\Shortcodes\Facades\Shortcode;

final class Bpanel4SliderServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-slider';

    public function boot(LivewireManager $livewire): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->commands([InstallCommand::class]);
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);
        $this->bindInterfaces();
        $this->registerLivewireComponents($livewire);
        $this->registerShortcode();
    }

    private function bindInterfaces(): void
    {
        $this->app->bind(SliderRepository::class, EloquentSliderRepository::class);
        $this->app->bind(SlideRepository::class, EloquentSlideRepository::class);
    }

    private function registerLivewireComponents(LivewireManager $livewireManager): void
    {
        $livewireManager->component(self::PACKAGE_PREFIX . '::slider-datatable', SlidersDatatable::class);
        $livewireManager->component(self::PACKAGE_PREFIX . '::slides-datatable', SlidesDatatable::class);
    }

    private function registerShortcode(): void
    {
        Shortcode::enable();
        Shortcode::register('bpanel4-slider', Bpanel4SliderShortcode::class);
    }
}
