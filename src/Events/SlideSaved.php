<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Slider\Events;

use Bittacora\Bpanel4\Slider\Models\Slide;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Request;

final class SlideSaved
{
    use Dispatchable;

    public function __construct(public readonly Slide $slide, public readonly Request $request)
    {
    }
}