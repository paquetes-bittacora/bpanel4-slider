<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {

    private const TABLE_NAME = 'sliders';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->id();
            $table->json('title');
            $table->boolean('show_pagination')->default(true);
            $table->boolean('show_arrows')->default(true);
            $table->integer('slide_duration')->default(5000);
            $table->integer('mobile_breakpoint')->default('640');
            $table->string('effect')->default('slide');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
