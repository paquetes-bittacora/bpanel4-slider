<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    private const TABLE_NAME = 'sliders_slides';

    public function up(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->dropColumn('background');
        });

        // Hago el borrado en 2 pasos porque SQLite no permite hacerlos simultáneamente y fallan los tests si los
        // pongo juntos
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->dropColumn('mobile_background');
        });
    }

    public function down(): void
    {
        Schema::table(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->string('background');
            $table->string('mobile_background')->nullable();
        });
    }
};
