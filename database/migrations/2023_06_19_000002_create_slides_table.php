<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {

    private const TABLE_NAME = 'sliders_slides';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('slider_id');
            $table->string('title');
            $table->json('content');
            $table->string('background');
            $table->string('mobile_background')->nullable();
            $table->json('link')->nullable();
            $table->boolean('new_tab')->nullable();
            $table->boolean('active')->default(true);
            $table->unsignedBigInteger('order_column')->nullable();
            $table->timestamps();

            $table->foreign('slider_id')->references('id')->on('sliders');
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
