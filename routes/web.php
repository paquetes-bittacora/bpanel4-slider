<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Slider\Http\Controllers\SlideAdminController;
use Bittacora\Bpanel4\Slider\Http\Controllers\SliderAdminController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel/sliders')->name('bpanel4-slider.bpanel.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(function (): void {
        Route::get('/', [SliderAdminController::class, 'index'])->name('index');
        Route::get('/nuevo', [SliderAdminController::class, 'create'])->name('create');
        Route::get('{model}/editar/{locale?}', [SliderAdminController::class, 'edit'])->name('edit');
        Route::get('/diapositiva/{slide}/quitar-fondo-movil', [SliderAdminController::class, 'removeMobileBackground'])
            ->name('remove-mobile-background');
        Route::post('/store', [SliderAdminController::class, 'store'])->name('store');
        Route::post('{slider}/actualizar', [SliderAdminController::class, 'update'])->name('update');
        Route::delete('{slider}/destroy', [SliderAdminController::class, 'destroy'])->name('destroy');
    });

Route::prefix('bpanel/sliders')->name('bpanel4-slider.bpanel.slide.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(function (): void {
        Route::get('/{slider}/nueva-diapositiva/{locale?}', [SlideAdminController::class, 'create'])->name('create');
        Route::get('/{model}/editar-diapositiva/{locale?}', [SlideAdminController::class, 'edit'])->name('edit');
        Route::post('/{slider}/store-slide', [SlideAdminController::class, 'store'])->name('store');
        Route::post('/{slide}/actualizar-diapositiva/{locale?}', [SlideAdminController::class, 'update'])->name('update');
        Route::delete('/{slide}/destroy-slide', [SlideAdminController::class, 'destroy'])->name('destroy');
    });
