# Insertar un slider

Para insertar un slider, se puede usar el siguiente shortcode:

```
[bpanel4-slider id=<id del slider>][/bpanel4-slider]
```

Se puede incluir en archivos blade, pero también en el contenido de una página o
entrada del blog (por ejemplo).

# Personalizar un slider

Como cada proyecto tendrá un estilo diferente para los slider, se incluyen únicamente
unas clases muy básicas que normalmente serán comunes.

Cada slider tiene en su elemento raíz la clase `.swiper-slider-<id del slider>`, que
permite personalizar con CSS la apariencia del mismo.

# Extender el módulo

Se han añadido un par de características para poder modificar el módulo sin alterar el código. De momento, se dispara el evento `\Bittacora\Bpanel4\Slider\Events\SlideSaved` cuando se crea o edita una diapositiva, y se usa el hook `slide-additional-fields` en el formulario de edición de cada diapositiva, para permitir que se añadan campos extra.

Para mostrar estos campos adicionales en el slider, está disponible el hook `show-slide-additional-fields`.